<?php
$data = file_get_contents("data/episodes.json");
$json_a = json_decode($data);
$seasons = $json_a->Seasons;
?>
<?php foreach ($seasons as $i=>$s): ?>
    <a style="text-decoration: none;"href="viewer.php?s=<?php echo $i; ?>">
        <div class="card seasons">
            <img src=<?php echo $s->thumb; ?> alt=<?php echo $s->name; ?>>
            <div class="overlay"></div>
            <h1><?php echo $s->name; ?></h1>
        </div>
    </a>
<?php endforeach; ?>
