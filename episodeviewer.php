<!DOCTYPE html>
<?php set_include_path('components'); ?>
<?php
$data = file_get_contents("data/episodes.json");
$json_a = json_decode($data);
if (isset($_GET["s"]) && $_GET["s"] <= count($json_a->Seasons)) {
    $season = $json_a->Seasons[$_GET["s"]];
    if (isset($_GET["e"]) && $_GET["e"] <= count($season->episodes)) {
        $episode = $season->episodes[$_GET["e"]];
    } else {
        redirect();
    }
} else {
    redirect();
}

function redirect() {
    header("Location: index.php");
    die();
}
?>
<html>
    <head>
        <?php include 'defaulthead.php'; ?>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    </head>
    <body>
        <?php include 'toolbar.php'; ?>
        <main class="container aftertoolbar">
            <a href="viewer.php?s=<?php echo $_GET["s"]; ?>">
                <h1 class="title"><?php echo $season->name;?></h1>
            </a>
            <h2 class="title">Episode <?php echo $episode->name;?></h2>

            <iframe src="<?php echo $episode->url;?>/preview" width="100%" height="480"
                frameBorder="0" allowfullscreen="true"></iframe>

        </main>
        <?php include 'footer.php';?>
    </body>

</html>
