<!DOCTYPE html>
<?php set_include_path('components'); ?>
<html>
    <head>
        <?php include 'defaulthead.php'; ?>
    </head>
    <body>
        <?php include 'toolbar.php'; ?>
        <main class="container carousel">
            <?php include 'seasonsviewer.php'; ?>
        </main>
        <?php include 'footer.php';?>

    </body>
</html>
