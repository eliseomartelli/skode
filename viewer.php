<!DOCTYPE html>
<?php set_include_path('components'); ?>
<?php
$data = file_get_contents("data/episodes.json");
$json_a = json_decode($data);
if (isset($_GET["s"]) && $_GET["s"] <= count($json_a->Seasons)) {

    $season = $json_a->Seasons[$_GET["s"]];
} else {
    redirect();
}
function redirect() {
    header("Location: index.php");
    die();
}
?>
<html>
    <head>
        <?php include 'defaulthead.php'; ?>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    </head>
    <body>
        <?php include 'toolbar.php'; ?>
        <main class="container aftertoolbar">

            <h1 class="title"><?php echo $season->name;?></h1>

            <?php foreach ($season->episodes as $i=>$s): ?>
                <a style="text-decoration: none;" href="episodeviewer.php?s=<?php echo $_GET["s"]; ?>&e=<?php echo $i;?>">
                    <div class="episodeviewer card">
                        <i class="material-icons">play_circle_filled</i>
                        <h3><?php echo $s->name; ?></h3>
                    </div>
                </a>
            <?php endforeach; ?>

        </main>
        <?php include 'footer.php';?>
    </body>

</html>
